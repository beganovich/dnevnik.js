
    // NPMs
    var express = require('express');
    var http = require('http');
    var path = require('path');
    var session = require('express-session');
    var mysql = require('mysql');
    var bodyParser = require('body-parser');
    var util = require('util');
    var cookieSession = require('cookie-session');
    var cookieParser = require('cookie-parser');
    var ejs = require('ejs');
    var request = require('request'); 

    // Config
    var config = require('./config.json'); 

    // Router
    var routes = require('./routes');
    var user = require('./routes/user');
    var auth = require('./routes/auth');
    var admin = require('./routes/admin');
    var api = require('./routes/api');

    // App
    var app = express(); 

    // MySQL 
    var mysqlconnection = mysql.createConnection({
        host: config.dbHost,
        user: config.dbUser,
        password: config.dbPassword,
        database: config.dbName
    });

    mysqlconnection.connect();
    global.db = mysqlconnection;

    // Application settings
    app.set('views', __dirname + '/views');
    app.set('view engine', 'ejs');
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(cookieSession({
        cookieName: 'session'
        , secret: 'secret'
        , httpOnly: true
        , ephemeral: true
        , duration: 30 * 60 * 1000
        , activeDuration: 5 * 60 * 1000
    }));
    app.use(cookieParser());

    // Routes
    app.use('/auth', auth);
    app.use('/api', api); 
    app.use('/admin', admin); 
    app.use('/', routes);
    app.use('/user', user);


    // 404
    app.use(function (req, res, next) {
        res.status(404).send('Oops, not found!');
    });

    // Server
    app.listen(8080, () => {
        console.log("We're up!");
    }); 