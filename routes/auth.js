
	var express = require('express')
	var router = express.Router(); 
	var util = require('util'); 
	var sha1 = require('sha1');
	var request = require('request');
	var config = require('./config.json');


	message = null;

	// Sign-in
	router.get('/sign-in/:message?', function(req, res) {

		let title = 'Prijava'; 
		let template = 'auth/sign-in.ejs'; 

		// Check if user is already logged in.
		if(req.session.dnevnikjs != null) {
			res.redirect('/auth/assign'); 
		} else if (req.params.message == 'error') {	
			res.render(template, {
				title: title,
				message: 'Uneseni podaci nisu ispravni!'
			}); 
		} else if(req.params.message == 'password-changed') {
			res.render(template, {
				title: title, 
				message: 'Lozinka promjenjena. Molimo prijavite se ponovo.'
			}); 
		} else {
			res.render(template, {
				title: title, 
				message: message
			})
		}

	}); 


	// Assign
	router.get('/assign', function(req, res) {

		// API
		request.post({ url: util.format("%s/api/auth/assign", config.www), form: {sessionId: req.session.dnevnikjs}}, function(err,httpResponse,body) {
			
			// Getting output
			let out = JSON.parse(body); 

			// Quick check
			if(out.message == 'session-admin') {
				res.redirect('/admin/pregled'); 
			} else if(out.message == 'session-user') {
				res.redirect('/user/ocjene'); 
			} else {
				res.redirect('/auth/sign-in');
			}
		}); 
	}); 

	// Sign-out
	router.get('/sign-out', function(req, res) {

		req.session = null;

	    if(req.session == null) {
	        res.redirect('/auth/sign-in')
	    } else {
	        res.send('CODENAME: COOKIE_ERROR');
	    }
	})

	// Export modules
	module.exports = router; 