
	var express = require('express'); 
	var router = express.Router(); 
	var request = require('request'); 
	var config = require('./config.json');
	var util = require('util');
	

	// User, marks
	router.get('/ocjene', function(req, res) {

		// Checking session
		if(req.session.dnevnikjs == null) {
			res.redirect('/auth/sign-in'); 
		} else if(req.session.dnevnikjs < 1) {
			res.redirect('/user/admin'); 
		} 

		// Getting info about user
		request.post({ url: util.format("%s/api/user/getname", config.www), form: {session: req.session.dnevnikjs}}, function(err, httpResponse, body) {

			// Info in JSON
			let out = JSON.parse(body); 

			// User name
			user = out[0].name; 


			// Get marks
			request.post({ url: util.format("%s/api/admin/student/marks", config.www), form: {data: req.session.dnevnikjs}}, function(err, httpResponse, outputMarks) {

				// JSON
				parseMarks = JSON.parse(outputMarks); 
				
				// Render
				res.render('user/ocjene.ejs', {title: 'Ocjene', subjects: parseMarks, name: user}); 

			}); 
		}); 

	});


	// User, meetings
	router.get('/sastanci', function(req, res) {

		// Checking session
		if(req.session.dnevnikjs == null) {
			res.redirect('/auth/sign-in'); 
		} else if(req.session.dnevnikjs < 1) {
			res.redirect('/user/admin'); 
		} 

		// Getting info about user
		request.post({ url: util.format("%s/api/user/getname", config.www), form: {session: req.session.dnevnikjs}}, function(err, httpResponse, body) {

			// Info in JSON
			let out = JSON.parse(body); 

			// User name
			user = out[0].name; 

			// Get meetings
			request.post({ url: util.format("%s/api/global/meetings", config.www), form: {}}, function(err, httpResponse, outputMeetings) {


				// JSON
				parseMeetings = JSON.parse(outputMeetings); 

				// Render
				res.render('user/sastanci.ejs', {title: 'Roditeljski sastanci', meetings: parseMeetings, name: user});

			}); 

		 }); 
	}); 


	// User, notes
	router.get('/napomene', function(req, res) {

		// Check session
		if (req.session.dnevnikjs == null) {
			res.redirect('/auth/sign-in');
		} else if (req.session.dnevnikjs < 1) {
			res.redirect('/user/admin');
		} 

		// Getting info about user
		request.post({ url: util.format("%s/api/user/getname", config.www), form: { session: req.session.dnevnikjs } }, function (err, httpResponse, body) {

			// Info in JSON
			let out = JSON.parse(body);

			// User name
			user = out[0].name;

			// Get notes
			request.post({ url: util.format("%s/api/global/meetings", config.www), form: {} }, function (err, httpResponse, outputMeetings) {


				// JSON
				parseMeetings = JSON.parse(outputMeetings);

				// Get Notes
				request.post({ url: util.format("%s/api/note/get", config.www), form: { sid: req.session.dnevnikjs } }, function (err, httpResponse, notesOutput) {

					// Parse notes
					parseNotes = JSON.parse(notesOutput);

					// Render
					res.render('user/napomene', { title: 'Napomene i bilješke', notes: parseNotes, name: user });

				});

			});

		}); 		

	})

	module.exports = router; 