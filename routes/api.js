
	var express = require('express'); 
	var router = express.Router(); 
	var util = require('util'); 
	var sha1 = require('sha1'); 
	var request = require('request'); 
	var config = require('./config.json');


	// Index
	router.get('/', function(req, res) {
		res.send('Hello, I am API, without documentation! :(');
	}); 	

	// Auth/Sign-in
	router.post('/auth/sign-in', function(req, res) {
		
		// User input from post
		let username = req.body.username; 
		let password = req.body.password; 

		// Query
		let sql = util.format("SELECT uid FROM logins WHERE username = '%s' AND password = '%s'", username, password); 

		// Execute query
		db.query(sql, function(err, result) {

			// If username/password combo is correct, set session
			if(result.length) {
				req.session.dnevnikjs = result[0].uid; 
				res.redirect('/auth/assign'); 
			}

			// .. else, return error
			else {
				res.redirect('/auth/sign-in/error'); 
			}
		}); 

	}); 

	// Auth

	// Auth/Assign
	router.post('/auth/assign', function(req, res) {
		
		// Value
		let session = req.body.sessionId; 

		// Checking
		if(session != 0) {
			res.send({message: 'session-user'}); 
		} else {
			res.send({message: 'session-admin'}); 
		}
	}); 


	// Admin/EditProfile
	router.post('/admin/profile/edit', function(req, res) {

		// Query
		let sql = util.format('UPDATE logins SET password = "%s" WHERE uid = "%s"', sha1(req.body.newpassword), req.body.uid); 
		db.query(sql, function(err, response) {
			if(response) {
				res.redirect('/auth/sign-out');
			}
		})
	}); 


	// Admin/GetAdminUsername
	router.post('/user/getname', function(req, res) {
			
		// Query
		let sql = util.format('SELECT name FROM logins WHERE uid = "%s"', req.body.session);
		db.query(sql, function(err, info) {
			res.send(info); 
		});  
	}); 


	// Admin/GetStudents
	router.post('/admin/students', function(req, res) {
			
		// Query
		let sql = util.format('SELECT uid, name, username, password FROM logins WHERE uid > 0');
		db.query(sql, function(err, info) {
			res.send(info); 
		});  
	}); 


	// Admin/Student
	router.post('/admin/student', function(req, res) {

		// Query
		let sql = util.format('INSERT INTO logins(name, username, password) VALUES("%s", "%s", "%s")', req.body.fullname, req.body.username, req.body.password); 
		db.query(sql); 
		res.redirect('/admin/pregled'); 
	}); 	


	// Admin/Student/Info
	router.post('/admin/student/info', function(req, res) {

		// Query
		let sql = util.format('SELECT uid, name, username, password FROM logins WHERE uid = "%s"', req.body.name);
		db.query(sql, function(err, info) {
			res.send(info); 
		}); 

	}); 


	// Admin/UpdateStudentInfo
	router.post('/update/student/password', function(req, res) {


		// Query
		let sql = util.format('UPDATE logins SET password = "%s" WHERE uid = "%s"', req.body.password, req.body.id); 
		db.query(sql, function(err, respond) {
			if(respond) {
				res.redirect('/admin/pregled');
			}
		}); 
	}); 


	// Admin/Student/Marks
	router.post('/admin/student/marks', function(req, res) {

		// Query
		let sql = util.format('SELECT hid, class, marks FROM marks WHERE sid = "%s"', req.body.data); 
		db.query(sql, function(err, marks) {
			res.send(marks); 
		})

	}); 


	// Admin/GetSubjects
	router.post('/admin/subjects', function(req, res) {

		// Query
		let sql = util.format('SELECT class FROM subjects'); 
		db.query(sql, function(err, info) {
			res.send(info); 
		}); 
	}); 


	// Admin/Subject
	router.post('/admin/subject', function(req, res) {

		// Query
		let sql = util.format("INSERT INTO subjects(class) VALUES('%s')", req.body.subject); 
		db.query(sql); 
		

		// Get all students
		request.post({url: util.format("%s/api/admin/students", config.www), form: {}}, function(err, httpResponse, outputStudents) {

			// JSON
			parseStudents = JSON.parse(outputStudents); 

			// ForEach
			parseStudents.forEach(function(student) {

				// SQL
				let sql = util.format("INSERT INTO marks(sid, class, marks) VALUES('%s', '%s', '%s')", student.uid, req.body.subject, ''); 
				db.query(sql);

			}); 

			res.redirect('/admin/predmeti');

		}); 

	}); 


	// Admin/Meetings
	router.post('/global/meetings', function(req, res) {

		// Query
		let sql = util.format('SELECT date, description FROM meetings'); 
		db.query(sql, function(err, meetings) {
			res.send(meetings); 
		}); 

	}); 

	// Admin/NewMeeting
	router.post('/admin/meeting/new', function(req, res) {

		// Query
		let sql = util.format('INSERT INTO meetings(date, description) VALUES("%s", "%s")', req.body.date, req.body.reason); 
		db.query(sql);
		res.redirect('/admin/sastanci'); 
	}); 


	// Admin/Update/Marks
	router.get('/update/marks/mark-:mid/subject-:sid/student-:id', function(req, res) {

		// Query, get current marks
		let sql = util.format('SELECT marks FROM marks WHERE hid = "%s" AND sid = "%s"', req.params.sid, req.params.id); 
		db.query(sql, function(err, response) {

			// Current marks
			let oldmarks = response[0].marks; 

			// New marks
			let newmarks = util.format("%s %s", oldmarks, req.params.mid); 

			// Update
			let sql = util.format('UPDATE marks SET marks = "%s" WHERE hid = "%s"', newmarks, req.params.sid);
			db.query(sql, function(err, response) {
				if(response) {
					res.redirect(util.format('/admin/pregled/%s', req.params.id));
				}
			})

		}); 
	}); 


	// Admin/Update/CustomMarks
	router.post('/update/CustomMarks', function(req, res) {

		// Query
		let sql = util.format('UPDATE marks SET marks = "%s" WHERE hid = "%s"', req.body.marks, req.body.hid); 
		db.query(sql, function(err, respond) {
			if(respond) {
				res.redirect(util.format('/admin/pregled/%s', req.body.sid)); 
			} else {
				res.send(err);
			}
		}); 

	}); 


	// Admin/DeleteMeeting
	router.post('/delete/meeting', function(req, res) {


		// Query
		let sql = util.format('DELETE FROM meetings WHERE date = "%s"', req.body.mid); 
		db.query(sql, function(err, respond) {
			if(respond) {
				res.redirect('/admin/sastanci');
			}
		}); 
	}); 

	// Updates
	router.post('/updates', function(req, res) {

		// Query
		let sql = util.format('SELECT version, description FROM updates ORDER BY uid DESC'); 
		db.query(sql, function(err, response) {
			res.send(response);
		}); 

	});

	// Note #new
	router.post('/note/new', function(req, res) {
		
		// Query
		let sql = util.format('INSERT INTO notes(sid, date, message) VALUES("%s", "%s", "%s")', req.body.sid, req.body.date, req.body.note);

		db.query(sql, function(err, respond) {
			if(respond) {
				res.redirect(util.format('/admin/pregled/%s', req.body.sid)); 
			} else {
				res.send(err);
			}
		})
		
	}); 

	// Note #get
	router.post('/note/get', function(req, res) {
		
		// Query
		let sql = util.format('SELECT date, message, nid FROM notes WHERE sid = "%s"', req.body.sid); 
		db.query(sql, function(err, respond) {
			res.send(respond);
		}) 

	}); 


	// Note #delete
	router.get('/note/delete/:id', function(req, res) {
		
		// Query
		let sql = util.format('DELETE FROM notes WHERE nid = "%s"', req.params.id);
		db.query(sql, function(err, respond) {
			if(respond) {
				res.redirect('/admin/pregled')
			} else {
				res.send(err);
			}
		}) 

	}); 


	

	// Exports module(s)
	module.exports = router; 