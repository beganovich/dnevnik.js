
	var express = require('express'); 
	var router = express.Router(); 
	var request = require('request');
	var util = require('util'); 
	var www = 'http://localhost:8080'; 
	var config = require('./config.json');
	
	message = null; 

	// Admin, dashboard
	router.get('/pregled', function(req, res) {

		// Checking session
		if(req.session.dnevnikjs == null) {
			res.redirect('/auth/sign-in'); 
		} else if(req.session.dnevnikjs > 0) {
			res.redirect('/user/pregled'); 
		} 

		// Getting info about admin
		request.post({ url: util.format("%s/api/user/getname", config.www), form: {session: req.session.dnevnikjs}}, function(err, httpResponse, body) {

			// Info in JSON
			let out = JSON.parse(body); 

			// Admin name
			admin = out[0].name; 
		}); 

		// Getting all students
		request.post({ url: util.format("%s/api/admin/students", config.www), form: {data: 'students'}}, function(err, httpResponse, studentsOutput) {

			// JSON
			let parseStudents = JSON.parse(studentsOutput); 

			// Convert body-response to JSON
			let students = parseStudents; 

			// ForEach
			res.render('admin/pregled.ejs', {title: 'Pregled dnevnika', admin: admin, students: students});  

		}); 

	}); 


	// Admin, student
	router.get('/pregled/:id', function(req, res) {

		// Checking session
		if(req.session.dnevnikjs == null) {
			res.redirect('/auth/sign-in'); 
		} else if(req.session.dnevnikjs > 0) {
			res.redirect('/user/pregled'); 
		} 

		// Getting info about admin
		request.post({ url: util.format("%s/api/user/getname", config.www), form: {session: req.session.dnevnikjs}}, function(err, httpResponse, body) {

			// Info in JSON
			let out = JSON.parse(body); 

			// Admin name
			admin = out[0].name; 
		}); 

		let sql = util.format('SELECT uid, name FROM logins WHERE uid = "%s"', req.params.id); 
		db.query(sql, function(err, response) {

			// .. if
			if(response.length) {
				

		// Get student name
		request.post({ url: util.format("%s/api/admin/student/info", config.www), form: {name: req.params.id}}, function(err, httpResponse, studentOutput) {

			// JSON
			parseStudent = JSON.parse(studentOutput); 

			// Get student marks
			request.post({ url: util.format("%s/api/admin/student/marks", config.www), form: {data: req.params.id}}, function(err, httpResponse, marksOutput) {

				// JSON
				parseMarks = JSON.parse(marksOutput); 

				// Get Notes
				request.post({ url: util.format("%s/api/note/get", config.www), form: {sid: req.params.id}}, function(err, httpResponse, notesOutput) {

					// Parse notes
					parseNotes = JSON.parse(notesOutput);
					
					// Render
					res.render('admin/pregled-id.ejs', { title: 'Pregled učenika', admin: admin, marks: parseMarks, name: parseStudent[0].name, sid: parseStudent[0].uid, username: parseStudent[0].username, password: parseStudent[0].password, notes: parseNotes}); 

				}); 
				
			}); 

		}); 

			} else {
				res.redirect('/admin/pregled');
			}

		}); 

	}); 

	// Admin, subjects
	router.get('/predmeti', function(req, res) {

		// Checking session
		if(req.session.dnevnikjs == null) {
			res.redirect('/auth/sign-in'); 
		} else if(req.session.dnevnikjs > 0) {
			res.redirect('/user/pregled'); 
		} 

		// Getting info about admin
		request.post({ url: util.format("%s/api/user/getname", config.www), form: {}}, function(err, httpResponse, body) {

			// Info in JSON
			let out = JSON.parse(body); 

			// Admin name
			admin = out[0].name; 
		}); 

		// Getting all subjects
		request.post({ url: util.format("%s/api/admin/subjects", config.www), form: {}}, function(err, httpResponse, subjectsOutput) {

			// JSON
			let parseSubjects = JSON.parse(subjectsOutput); 

			// body-response
			let subjects = parseSubjects; 

			// ForEach
			res.render('admin/predmeti.ejs', {title: 'Predmeti', admin: admin, subjects: subjects}); 

		}); 

	}); 

	// Admin, meetings
	router.get('/sastanci', function(req, res) {

			// Checking session
			if(req.session.dnevnikjs == null) {
				res.redirect('/auth/sign-in'); 
			} else if(req.session.dnevnikjs > 0) {
				res.redirect('/user/pregled'); 
			} 

			// Getting info about admin
			request.post({ url: util.format("%s/api/user/getname", config.www), form: {}}, function(err, httpResponse, body) {

				// Info in JSON
				let out = JSON.parse(body); 

				// Admin name
				admin = out[0].name; 
			}); 

		// Getting all meetings
		request.post({ url: util.format("%s/api/global/meetings", config.www), form: {}}, function(err, httpResponse, meetingsOutput) {


			// JSON
			let parseMeetings = JSON.parse(meetingsOutput); 

			// body-response
			let meetings = parseMeetings; 

			// Render
			res.render('admin/sastanci.ejs', {title: 'Roditeljski sastanci', admin: admin, meetings: meetings}); 

		}); 
	}); 

	// Admin, updates
	router.get('/verzije', function(req, res) {

		// Checking session
		if(req.session.dnevnikjs == null) {
			res.redirect('/auth/sign-in'); 
		} else if(req.session.dnevnikjs > 0) {
			res.redirect('/user/pregled'); 
		} 

		// Getting info about admin
		request.post({url: util.format("%s/api/updates", config.www), form: {}}, function(err, httpResponse, body) {

			// Info in JSON
			let out = JSON.parse(body); 

			// Admin name
			admin = out[0].name; 
		}); 


		// Getting info about admin
		let apiupdates = util.format('%s/api/updates', www);
		request.post({url: apiupdates, form: {}}, function(err, httpResponse, updates) {

			// Info in JSON
			let parseUpdates = JSON.parse(updates); 

			// Admin name
			res.render('admin/verzije.ejs', {title: 'Verzije i ažuriranja', updates: parseUpdates, admin: admin});
		}); 



	}); 

	// Export module(s)
	module.exports = router; 