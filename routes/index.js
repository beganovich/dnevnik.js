var express = require('express'); 
var router = express.Router(); 

router.get('/', function(req, res) {
	
	// Checking session
	if(req.session.dnevnikjs == undefined ) {
		res.redirect('/auth/sign-in'); 
	} else {
		res.redirect('/auth/assign');
	}

}); 

module.exports = router;